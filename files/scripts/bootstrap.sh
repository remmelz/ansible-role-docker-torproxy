#!/bin/bash

###################

apt-get -y update
apt-get -y install \
  qrencode \
  curl \
  tor \
  nginx

###################

mkdir -p /srv/www
echo 'please wait...' > /srv/www/index.html
chown -R www-data. /srv/www

###################

cd /var/tmp/nginx
for config in `ls -1`; do

  [[ -z `grep 'tor_friendly' ${config}` ]] && continue

  service=`echo ${config} \
    | awk -F'_' '{print $3}' \
    | awk -F'.' '{print $1}'`

  service_target=`grep 'proxy_pass ' ${config} \
    | awk -F' ' '{print $2}' \
    | sed 's/;//g' \
    | sed 's|http://||g'`

  [[ -z ${service_target} ]] && continue

  echo "HiddenServiceDir  /var/lib/tor/${service}/" >> /etc/tor/torrc
  echo "HiddenServicePort 80 ${service_target}"     >> /etc/tor/torrc
  echo                                              >> /etc/tor/torrc
  
done

###################

cp -v /var/tmp/nginx.conf /etc/nginx/sites-available/tor.conf
cp -Rv /var/tmp/images /srv/www/img
cd /etc/nginx/sites-enabled && rm default
ln -s ../sites-available/tor.conf

