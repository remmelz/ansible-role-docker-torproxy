#!/bin/bash

tor_txt="/srv/www/onions.txt"
tor_conf="/etc/tor/torrc"

chown debian-tor /var/lib/tor

service nginx start; sleep 4
service tor start;   sleep 4

cd /var/lib/tor

while true; do

  > ${tor_txt}

  for hiddendir in `cat ${tor_conf} \
    | grep '^HiddenServiceDir' \
    | awk -F' ' '{print $2}'`; do
  
    onion_service=`basename ${hiddendir}`
    onion_url=`cat ${hiddendir}/hostname`

    target_ip=`cat ${tor_conf} \
      | grep '^HiddenServicePort' \
      | grep _${onion_service} \
      | awk -F' ' '{print $3}'`
  
    status="offline"
    curl -s ${target_ip} > /dev/null
    [[ $? == 0 ]] && status="online"
  
    qrencode -s 4 -l M \
      -o /srv/www/img/${onion_service}.png \
      "http://${onion_url}"
  
    echo "${onion_service} ${onion_url} ${status}"
    echo "${onion_service} ${onion_url} ${status}" >> ${tor_txt}
    
  done

  sleep 60

done

